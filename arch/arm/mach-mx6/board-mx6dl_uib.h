/*
 * Copyright (C) 2012-2013 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef _BOARD_MX6DL_SABRESD_H
#define _BOARD_MX6DL_SABRESD_H
#include <mach/iomux-mx6dl.h>

static iomux_v3_cfg_t mx6dl_sabresd_pads[] = {
	/* AUDMUX */
	MX6DL_PAD_CSI0_DAT4__AUDMUX_AUD3_TXC,
	MX6DL_PAD_CSI0_DAT5__AUDMUX_AUD3_TXD,
	MX6DL_PAD_CSI0_DAT6__AUDMUX_AUD3_TXFS,
	MX6DL_PAD_CSI0_DAT7__AUDMUX_AUD3_RXD,

	/* RFID ECSPI */
	MX6DL_PAD_DISP0_DAT20__ECSPI1_SCLK,
	MX6DL_PAD_DISP0_DAT21__ECSPI1_MOSI,
	MX6DL_PAD_DISP0_DAT22__ECSPI1_MISO,
	MX6DL_PAD_DISP0_DAT23__ECSPI1_SS0,
	/* RFID control */
	MX6DL_PAD_EIM_D30__GPIO_3_30,
	MX6DL_PAD_EIM_D31__GPIO_3_31,
	MX6DL_PAD_DISP0_DAT8__GPIO_4_29,
	MX6DL_PAD_DISP0_DAT9__GPIO_4_30,

	/* ENET */
	MX6DL_PAD_ENET_MDIO__ENET_MDIO,
	MX6DL_PAD_ENET_MDC__ENET_MDC,
	MX6DL_PAD_RGMII_TXC__ENET_RGMII_TXC,
	MX6DL_PAD_RGMII_TD0__ENET_RGMII_TD0,
	MX6DL_PAD_RGMII_TD1__ENET_RGMII_TD1,
	MX6DL_PAD_RGMII_TD2__ENET_RGMII_TD2,
	MX6DL_PAD_RGMII_TD3__ENET_RGMII_TD3,
	MX6DL_PAD_RGMII_TX_CTL__ENET_RGMII_TX_CTL,
	MX6DL_PAD_ENET_REF_CLK__ENET_TX_CLK,
	MX6DL_PAD_RGMII_RXC__ENET_RGMII_RXC,
	MX6DL_PAD_RGMII_RD0__ENET_RGMII_RD0,
	MX6DL_PAD_RGMII_RD1__ENET_RGMII_RD1,
	MX6DL_PAD_RGMII_RD2__ENET_RGMII_RD2,
	MX6DL_PAD_RGMII_RD3__ENET_RGMII_RD3,
	MX6DL_PAD_RGMII_RX_CTL__ENET_RGMII_RX_CTL,
	MX6DL_PAD_ENET_CRS_DV__GPIO_1_25,	/* Micrel RGMII Phy Interrupt */
	MX6DL_PAD_ENET_RXD1__GPIO_1_26,		/* RGMII Interrupt */

	/* I2C1 */
	MX6DL_PAD_CSI0_DAT8__I2C1_SDA,
	MX6DL_PAD_CSI0_DAT9__I2C1_SCL,

	/* I2C2 */
	MX6DL_PAD_KEY_COL3__I2C2_SCL,
	MX6DL_PAD_KEY_ROW3__I2C2_SDA,

	/* I2C3 */
	MX6DL_PAD_GPIO_3__I2C3_SCL,
	MX6DL_PAD_GPIO_6__I2C3_SDA,

	/* DISPLAY */
	MX6DL_PAD_DI0_DISP_CLK__IPU1_DI0_DISP_CLK,
	MX6DL_PAD_DI0_PIN15__IPU1_DI0_PIN15,		/* DE */
	MX6DL_PAD_DI0_PIN2__IPU1_DI0_PIN2,		/* HSync */
	MX6DL_PAD_DI0_PIN3__IPU1_DI0_PIN3,		/* VSync */
	MX6DL_PAD_DI0_PIN4__IPU1_DI0_PIN4,		/* Contrast */
	MX6DL_PAD_DISP0_DAT0__IPU1_DISP0_DAT_0,
	MX6DL_PAD_DISP0_DAT1__IPU1_DISP0_DAT_1,
	MX6DL_PAD_DISP0_DAT2__IPU1_DISP0_DAT_2,
	MX6DL_PAD_DISP0_DAT3__IPU1_DISP0_DAT_3,
	MX6DL_PAD_DISP0_DAT4__IPU1_DISP0_DAT_4,
	MX6DL_PAD_DISP0_DAT5__IPU1_DISP0_DAT_5,
	MX6DL_PAD_DISP0_DAT6__IPU1_DISP0_DAT_6,
	MX6DL_PAD_DISP0_DAT7__IPU1_DISP0_DAT_7,
	MX6DL_PAD_DISP0_DAT10__IPU1_DISP0_DAT_10,
	MX6DL_PAD_DISP0_DAT11__IPU1_DISP0_DAT_11,
	MX6DL_PAD_DISP0_DAT12__IPU1_DISP0_DAT_12,
	MX6DL_PAD_DISP0_DAT13__IPU1_DISP0_DAT_13,
	MX6DL_PAD_DISP0_DAT14__IPU1_DISP0_DAT_14,
	MX6DL_PAD_DISP0_DAT15__IPU1_DISP0_DAT_15,
	MX6DL_PAD_DISP0_DAT16__IPU1_DISP0_DAT_16,
	MX6DL_PAD_DISP0_DAT17__IPU1_DISP0_DAT_17,
	MX6DL_PAD_DISP0_DAT18__IPU1_DISP0_DAT_18,
	MX6DL_PAD_DISP0_DAT19__IPU1_DISP0_DAT_19,

	/* FlexCAN */
	MX6DL_PAD_GPIO_7__CAN1_TXCAN,
	MX6DL_PAD_GPIO_8__CAN1_RXCAN,
	/* CAN1_STBY */
	MX6DL_PAD_GPIO_19__GPIO_4_5,

	/* UART1 for debug */
	MX6DL_PAD_CSI0_DAT10__UART1_TXD,
	MX6DL_PAD_CSI0_DAT11__UART1_RXD,

	/* USB */
	/* USB_OTG_PWR_EN */
	MX6DL_PAD_EIM_D22__GPIO_3_22,
	MX6DL_PAD_EIM_D21__USBOH3_USBOTG_OC,
	/*USB_H1 PWR EN*/
	MX6DL_PAD_ENET_TXD1__GPIO_1_29,

	/* USDHC2 */
	MX6DL_PAD_SD2_CLK__USDHC2_CLK,
	MX6DL_PAD_SD2_CMD__USDHC2_CMD,
	MX6DL_PAD_SD2_DAT0__USDHC2_DAT0,
	MX6DL_PAD_SD2_DAT1__USDHC2_DAT1,
	MX6DL_PAD_SD2_DAT2__USDHC2_DAT2,
	MX6DL_PAD_SD2_DAT3__USDHC2_DAT3,

	/* USDHC3 */
	MX6DL_PAD_SD3_CLK__USDHC3_CLK_50MHZ,
	MX6DL_PAD_SD3_CMD__USDHC3_CMD_50MHZ,
	MX6DL_PAD_SD3_DAT0__USDHC3_DAT0_50MHZ,
	MX6DL_PAD_SD3_DAT1__USDHC3_DAT1_50MHZ,
	MX6DL_PAD_SD3_DAT2__USDHC3_DAT2_50MHZ,
	MX6DL_PAD_SD3_DAT3__USDHC3_DAT3_50MHZ,
	MX6DL_PAD_SD3_DAT4__USDHC3_DAT4_50MHZ,
	MX6DL_PAD_SD3_DAT5__USDHC3_DAT5_50MHZ,
	MX6DL_PAD_SD3_DAT6__USDHC3_DAT6_50MHZ,
	MX6DL_PAD_SD3_DAT7__USDHC3_DAT7_50MHZ,

	/* HDMI_CEC_IN*/
	MX6DL_PAD_KEY_ROW2__HDMI_TX_CEC_LINE,

	/* info/wake */
	MX6DL_PAD_EIM_D29__GPIO_3_29,
	/* power off */
	MX6DL_PAD_EIM_D28__GPIO_3_28,
	/* boot config */
	MX6DL_PAD_EIM_DA0__GPIO_3_0,
	MX6DL_PAD_EIM_DA1__GPIO_3_1,
	MX6DL_PAD_EIM_DA2__GPIO_3_2,
	MX6DL_PAD_EIM_DA3__GPIO_3_3,
	MX6DL_PAD_EIM_DA4__GPIO_3_4,
	MX6DL_PAD_EIM_DA5__GPIO_3_5,
	MX6DL_PAD_EIM_DA6__GPIO_3_6,
	MX6DL_PAD_EIM_DA7__GPIO_3_7,
	MX6DL_PAD_EIM_DA8__GPIO_3_8,
	MX6DL_PAD_EIM_DA9__GPIO_3_9,
	MX6DL_PAD_EIM_DA10__GPIO_3_10,
	MX6DL_PAD_EIM_DA11__GPIO_3_11,
	MX6DL_PAD_EIM_DA12__GPIO_3_12,
	MX6DL_PAD_EIM_DA13__GPIO_3_13,
	MX6DL_PAD_EIM_DA14__GPIO_3_14,
	MX6DL_PAD_EIM_DA15__GPIO_3_15,
	MX6DL_PAD_EIM_A16__GPIO_2_22,
	MX6DL_PAD_EIM_A17__GPIO_2_21,
	MX6DL_PAD_EIM_A18__GPIO_2_20,
	MX6DL_PAD_EIM_A19__GPIO_2_19,
	MX6DL_PAD_EIM_A20__GPIO_2_18,
	MX6DL_PAD_EIM_A21__GPIO_2_17,
	MX6DL_PAD_EIM_A22__GPIO_2_16,
	MX6DL_PAD_EIM_A23__GPIO_6_6,
	MX6DL_PAD_EIM_A24__GPIO_5_4,
	MX6DL_PAD_EIM_EB0__GPIO_2_28,
	MX6DL_PAD_EIM_EB1__GPIO_2_29,
	MX6DL_PAD_EIM_EB2__GPIO_2_30,
	MX6DL_PAD_EIM_EB3__GPIO_2_31,

	/* PCIE_PWR_EN */
	MX6DL_PAD_EIM_D19__GPIO_3_19,
	/* PCIE_WAKE_B */
	MX6DL_PAD_CSI0_DATA_EN__GPIO_5_20,
	/* PCIE_DIS_B */
	MX6DL_PAD_KEY_COL4__GPIO_4_14,

	/*WDOG_B to reset pmic*/
	MX6DL_PAD_GPIO_1__WDOG2_WDOG_B,

	/* LED1 */
	MX6DL_PAD_GPIO_5__GPIO_1_5,
	/* LED2 */
	MX6DL_PAD_GPIO_7__GPIO_1_7,
	/* LED3 */
	MX6DL_PAD_GPIO_8__GPIO_1_8,

	/* DISP_PWM */
	MX6DL_PAD_GPIO_9__PWM1_PWMO,

	/* LVDS0 EN */
	MX6DL_PAD_GPIO_17__GPIO_7_12,
	/* LCD CNTRL_VGH */
	MX6DL_PAD_SD4_DAT2__GPIO_2_10,

	/* PWM speaker */
	MX6DL_PAD_SD4_DAT1__PWM3_PWMO,

	/* OSD/SSD2543 touch panel IRQ */
	MX6DL_PAD_EIM_D23__GPIO_3_23,
	/* OSD/SSD2543 touch panel reset */
	MX6DL_PAD_EIM_D24__GPIO_3_24,

	/* GPMI NAND */
	MX6DL_PAD_NANDF_CLE__RAWNAND_CLE,
	MX6DL_PAD_NANDF_ALE__RAWNAND_ALE,
	MX6DL_PAD_NANDF_CS0__RAWNAND_CE0N,
	MX6DL_PAD_NANDF_CS1__RAWNAND_CE1N,
	MX6DL_PAD_NANDF_RB0__RAWNAND_READY0,
	MX6DL_PAD_NANDF_D0__RAWNAND_D0,
	MX6DL_PAD_NANDF_D1__RAWNAND_D1,
	MX6DL_PAD_NANDF_D2__RAWNAND_D2,
	MX6DL_PAD_NANDF_D3__RAWNAND_D3,
	MX6DL_PAD_NANDF_D4__RAWNAND_D4,
	MX6DL_PAD_NANDF_D5__RAWNAND_D5,
	MX6DL_PAD_NANDF_D6__RAWNAND_D6,
	MX6DL_PAD_NANDF_D7__RAWNAND_D7,
	MX6DL_PAD_SD4_CMD__RAWNAND_RDN,
	MX6DL_PAD_SD4_CLK__RAWNAND_WRN,
	MX6DL_PAD_NANDF_WP_B__RAWNAND_RESETN,
};

static iomux_v3_cfg_t mx6dl_sabresd_hdmi_ddc_pads[] = {
	MX6DL_PAD_KEY_COL3__HDMI_TX_DDC_SCL, /* HDMI DDC SCL */
	MX6DL_PAD_KEY_ROW3__HDMI_TX_DDC_SDA, /* HDMI DDC SDA */
};

static iomux_v3_cfg_t mx6dl_sabresd_i2c2_pads[] = {
	MX6DL_PAD_KEY_COL3__I2C2_SCL,	/* I2C2 SCL */
	MX6DL_PAD_KEY_ROW3__I2C2_SDA,	/* I2C2 SDA */
};
#endif
